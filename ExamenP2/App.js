import React, { useState, useEffect } from 'react';
import { View, TextInput, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { initializeApp } from 'firebase/app';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, onAuthStateChanged, signOut } from 'firebase/auth';
import CustomButton from './CustomButton'; 
// Configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyDGhv2M9uRf0OMwliR99_cGY39MUmJEBio",
  authDomain: "desarrollomovil-96943.firebaseapp.com",
  projectId: "desarrollomovil-96943",
  storageBucket: "desarrollomovil-96943.appspot.com",
  messagingSenderId: "899645340534",
  appId: "1:899645340534:web:95edddfd8d9271c6b05857",
  measurementId: "G-TLVMHHNGH0"
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

export default function App() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const [isLogin, setIsLogin] = useState(true);

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
      setLoading(false);
    });

    return () => unsubscribe();
  }, []);

  const handleAuthentication = async () => {
    try {
      if (isLogin) {
        await signInWithEmailAndPassword(auth, email, password);
      } else {
        await createUserWithEmailAndPassword(auth, email, password);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSignOut = async () => {
    try {
      await signOut(auth);
    } catch (error) {
      console.error(error);
    }
  };

  if (loading) {
    return <ActivityIndicator size="large" />;
  }

  if (user) {
    return (
      <View style={styles.container}>
        <Text>Welcome, {user.email}</Text>
        <CustomButton title="Sign Out" onPress={handleSignOut} buttonStyle={styles.button} textStyle={styles.buttonText} />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{isLogin ? 'Sign In' : 'Sign Up'}</Text>
      <TextInput
        style={styles.input}
        value={email}
        onChangeText={setEmail}
        placeholder="Email"
        autoCapitalize="none"
      />
      <TextInput
        style={styles.input}
        value={password}
        onChangeText={setPassword}
        placeholder="Password"
        secureTextEntry
      />
      <CustomButton
        title={isLogin ? 'Sign In' : 'Sign Up'}
        onPress={handleAuthentication}
        buttonStyle={styles.button}
        textStyle={styles.buttonText}
      />
      <Text style={styles.toggleText} onPress={() => setIsLogin(!isLogin)}>
        {isLogin ? 'Need an account? Sign Up' : 'Already have an account? Sign In'}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
    padding: 16,
  },
  title: {
    fontSize: 24,
    marginBottom: 60,
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#333',
  },
  input: {
    width: '100%',
    height: 40,
    borderColor: '#ccc',
    borderWidth: 0,
    borderRadius: 20,
    marginBottom: 12,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
  },
  button: {
    marginTop: 50,
    backgroundColor: '#8a2be2', 
    borderRadius: 20, 
    width: '30%',
    height: '10px',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  toggleText: {
    color: '#8a2be2',
    marginTop: 10,
  },
  error: {
    color: 'red',
    marginTop: 10,
  },
});
